package br.com.sicredi.eventos.data.utils

import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.core.left
import br.com.sicredi.eventos.core.rigth
import retrofit2.Response

class ResponseValidator {

    companion object {
        @JvmStatic
        fun <T> validate(response: Response<T>): Either<Exception, T> {
            val successStatus = (200..299).toList().toIntArray()
            val failStatus = (400..499).toList().toIntArray()
            val serverErrorStatus = (500..599).toList().toIntArray()

            if (successStatus.contains(response.code())) {
                return rigth(response.body()!!)
            }

            if (failStatus.contains(response.code()))
                return left(
                    Exception(
                        response.code()
                            .toString() + " [" + response.message() + "] " + " \n " + response.errorBody()
                            ?.string()
                    )
                )

            if (serverErrorStatus.contains(response.code()))
                return left(
                    Exception(
                        response.code()
                            .toString() + " [" + response.message() + "] " + " \n " + response.errorBody()
                            ?.string()
                    )
                )

            return left(Exception("Erro desconhecido [no response]"))
        }
    }

}