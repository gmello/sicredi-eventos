package br.com.sicredi.eventos

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import br.com.sicredi.eventos.core.rigth
import br.com.sicredi.eventos.data.IEventsRepository
import br.com.sicredi.eventos.data.model.CheckinRequest
import br.com.sicredi.eventos.data.model.EventResponse
import br.com.sicredi.eventos.domain.checkin.CheckinUseCase
import br.com.sicredi.eventos.domain.event.EventShareContentUseCase
import br.com.sicredi.eventos.domain.home.HomeGetEventsUseCase
import br.com.sicredi.eventos.domain.model.Event
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import java.io.File

@ExperimentalCoroutinesApi
class UseCaseUnitTest {

    @Test
    fun `assert home get events use case return correct type and elements size`() =
        runBlockingTest {
            val mockRepository = mockk<IEventsRepository>()
            val homeGetEventsUseCase = HomeGetEventsUseCase(mockRepository)

            coEvery { mockRepository.getEvents() } returns rigth(
                listOf(
                    EventResponse(
                        people = emptyList(),
                        date = 123,
                        description = "test",
                        image = "test",
                        longitude = 1.0,
                        latitude = 1.0,
                        price = 1.0,
                        title = "test",
                        id = "test"
                    ),
                    EventResponse(
                        people = emptyList(),
                        date = 123,
                        description = "test",
                        image = "test",
                        longitude = 1.0,
                        latitude = 1.0,
                        price = 1.0,
                        title = "test",
                        id = "test"
                    ),
                )
            )

            val result = homeGetEventsUseCase.execute()

            assertEquals(
                "should be a Rigth type",
                result.isRigth(),
                true
            )

            result.foldExecute({

            }, {
                assertEquals(
                    "should be 2 events result",
                    it.size,
                    2
                )
            })

        }

    @Test
    fun `assert event share use case return a send type intent`() = runBlockingTest {

        val eventShareContentUseCase = EventShareContentUseCase()
        val eventMock = Event(
            people = emptyList(),
            date = 123,
            description = "test",
            image = "test",
            longitude = 1.0,
            latitude = 1.0,
            price = 1.0,
            title = "test",
            id = "test"
        )

        val bitmap = mockk<Bitmap>()
        val file = mockk<File>()
        val context = mockk<Context>()

        val result = eventShareContentUseCase.execute(eventMock, bitmap, file, context)

        result.foldExecute({

        }, {
            assertEquals(
                "should be a action view intent",
                it.action,
                Intent.ACTION_SEND
            )
        })

    }

    @Test
    fun `assert checkin validate event id`() = runBlockingTest {
        val mockRepository = mockk<IEventsRepository>()
        val checkinUseCase = CheckinUseCase(mockRepository)

        val mockRequestCheckin = CheckinRequest(
            eventId = -1,
            name = "",
            email = ""
        )

        val result = checkinUseCase.execute(mockRequestCheckin)

        result.foldExecute({
            assertEquals(
                "should be greater than 0",
                it.message,
                "Código do evento não foi informado!"
            )
        }, {})

    }

    @Test
    fun `assert checkin validate name`() = runBlockingTest {
        val mockRepository = mockk<IEventsRepository>()
        val checkinUseCase = CheckinUseCase(mockRepository)

        val mockRequestCheckin = CheckinRequest(
            eventId = 10,
            name = "",
            email = ""
        )

        val result = checkinUseCase.execute(mockRequestCheckin)

        result.foldExecute({
            assertEquals(
                "name should be defined",
                it.message,
                "Preencha o nome!"
            )
        }, {})
    }

    @Test
    fun `assert checkin validate email`() = runBlockingTest {
        val mockRepository = mockk<IEventsRepository>()
        val checkinUseCase = CheckinUseCase(mockRepository)

        val mockRequestCheckin = CheckinRequest(
            eventId = 10,
            name = "Gustavo",
            email = ""
        )

        val result = checkinUseCase.execute(mockRequestCheckin)

        result.foldExecute({
            assertEquals(
                "email should be defined",
                it.message,
                "Email inválido, verifique por favor!"
            )
        }, {})
    }
}