package br.com.sicredi.eventos.domain.checkin

import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.data.model.CheckinRequest
import br.com.sicredi.eventos.data.model.CheckinResponse

interface ICheckinUseCase {
    suspend fun execute(request: CheckinRequest): Either<Exception, CheckinResponse>
}