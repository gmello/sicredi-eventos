package br.com.sicredi.eventos.presenter.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import br.com.sicredi.eventos.BR
import br.com.sicredi.eventos.R
import br.com.sicredi.eventos.databinding.CheckinBottomBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CheckinBottomSheet(
    private val viewModel: EventViewModel
) : BottomSheetDialogFragment() {

    companion object {
        const val TAG = "ModalBottomSheet"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: CheckinBottomBinding = DataBindingUtil.inflate(layoutInflater, R.layout.bottom_sheet_event_checkin, container, false)

        binding.setVariable(BR.viewModel, viewModel)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.executePendingBindings()

        return binding.root
    }

}