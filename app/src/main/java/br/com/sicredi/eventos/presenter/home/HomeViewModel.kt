package br.com.sicredi.eventos.presenter.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.sicredi.eventos.data.model.EventResponse
import br.com.sicredi.eventos.domain.home.IHomeGetEventsUseCase
import br.com.sicredi.eventos.presenter.model.Event
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(
    private val homeGetEventsUseCase: IHomeGetEventsUseCase,
    private val mainDispatcher: CoroutineDispatcher,
) : ViewModel() {

    val emitError: MutableLiveData<String> = MutableLiveData("")
    val emitEvents: MutableLiveData<List<Event>> = MutableLiveData(emptyList())
    val emitLoading: MutableLiveData<Boolean> = MutableLiveData(false)

    fun loadEvents(){
        emitLoading.value = true
        viewModelScope.launch {
            try {
                val result = homeGetEventsUseCase.execute()
                val events: MutableList<Event> = mutableListOf()

                withContext(mainDispatcher) {

                    result.foldExecute({
                        emitError.value = it.message
                    }, {
                        events.addAll(it.map { event -> Event.from(event) })
                    })

                    emitEvents.value = events
                    emitLoading.value = false
                }
            } catch (e: Exception) {
                withContext(mainDispatcher) {
                    emitLoading.value = false
                    emitError.value = e.message
                }
            }
        }
    }

}