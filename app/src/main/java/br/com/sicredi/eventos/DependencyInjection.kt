package br.com.sicredi.eventos

import br.com.sicredi.eventos.data.EventsRepository
import br.com.sicredi.eventos.data.IEventsApi
import br.com.sicredi.eventos.data.IEventsRepository
import br.com.sicredi.eventos.domain.checkin.CheckinUseCase
import br.com.sicredi.eventos.domain.checkin.ICheckinUseCase
import br.com.sicredi.eventos.domain.event.*
import br.com.sicredi.eventos.domain.home.HomeGetEventsUseCase
import br.com.sicredi.eventos.domain.home.IHomeGetEventsUseCase
import br.com.sicredi.eventos.presenter.checkinsuccess.CheckinSuccessViewModel
import br.com.sicredi.eventos.presenter.event.EventViewModel
import br.com.sicredi.eventos.presenter.home.HomeViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val viewModelModule = module {
    viewModel { HomeViewModel(get() , get(named("mainDispatcher"))) }
    viewModel { EventViewModel(get(), get(), get(), get(named("mainDispatcher"))) }
    viewModel { CheckinSuccessViewModel() }
}

val networkModule = module {
    factory { providesOkHttpClient() }
    factory { providesEventsApi(get()) }
    single { providesRetrofit(get()) }
}

val repositoryModule = module {
    single { EventsRepository(get()) as IEventsRepository }
}

val userCaseModule = module {
    single { HomeGetEventsUseCase(get()) as IHomeGetEventsUseCase }
    single { EventDetailsUseCase(get()) as IEventDetailsUseCase }
    single { CheckinUseCase(get()) as ICheckinUseCase }
    single { EventShareContentUseCase() as IEventShareContentUseCase }
    single { EventDirectionUseCase() as IEventDirectionUseCase }
}

val coroutinesModule = module {
    factory(named("defaultDispatcher")) { providesDefaultDispatcher() }
    factory(named("ioDispatcher")) { providesIoDispatcher() }
    factory(named("mainDispatcher")) { providesMainDispatcher() }
}

fun providesDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default
fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO
fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main

fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://5f5a8f24d44d640016169133.mockapi.io/api/")
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

fun providesOkHttpClient(): OkHttpClient {
    return OkHttpClient().newBuilder()
        .callTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(30, TimeUnit.SECONDS)
        .build()
}

fun providesEventsApi(retrofit: Retrofit): IEventsApi = retrofit.create(IEventsApi::class.java)
