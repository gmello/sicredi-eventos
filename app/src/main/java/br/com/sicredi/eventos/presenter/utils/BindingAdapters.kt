package br.com.sicredi.eventos.presenter.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

class BindingAdapters {
    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl", "error")
        fun loadImage(view: ImageView, url: String, error: Drawable) {
            Picasso
                .get()
                .load(url)
                .fit()
                .error(error)
                .into(view)
        }

        @JvmStatic
        @BindingAdapter("hideKeyboardOnClick")
        fun hideKeyboardOnClick(view: View, hideKeyboard: Boolean){
            if(hideKeyboard){
                view.setOnClickListener {
                    val imm: InputMethodManager? =
                        view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm?.hideSoftInputFromWindow(view.windowToken, 0)
                }
            }
        }

    }
}