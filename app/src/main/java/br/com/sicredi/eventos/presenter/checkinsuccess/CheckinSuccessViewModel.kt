package br.com.sicredi.eventos.presenter.checkinsuccess

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CheckinSuccessViewModel : ViewModel() {

    val emitDone = MutableLiveData<Boolean>()

    fun done(){
        emitDone.value = true
    }


}