package br.com.sicredi.eventos.data

import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.data.model.CheckinRequest
import br.com.sicredi.eventos.data.model.CheckinResponse
import br.com.sicredi.eventos.data.model.EventResponse

interface IEventsRepository {
    suspend fun getEvents(): Either<Exception, List<EventResponse>>
    suspend fun getEventDetails(id: Int): Either<Exception, EventResponse>
    suspend fun checkin(request: CheckinRequest): Either<Exception, CheckinResponse>
}