package br.com.sicredi.eventos.presenter.event

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.sicredi.eventos.data.model.CheckinRequest
import br.com.sicredi.eventos.domain.checkin.ICheckinUseCase
import br.com.sicredi.eventos.domain.event.IEventDirectionUseCase
import br.com.sicredi.eventos.domain.event.IEventShareContentUseCase
import br.com.sicredi.eventos.presenter.model.Event
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File


class EventViewModel(
    private val shareContentUseCase: IEventShareContentUseCase,
    private val eventDirectionUseCase: IEventDirectionUseCase,
    private val checkinUseCase: ICheckinUseCase,
    private val mainDispatcher: CoroutineDispatcher,
) : ViewModel() {

    val emitDirection = MutableLiveData<Intent?>()
    val emitShare = MutableLiveData<Intent?>()
    val emitError = MutableLiveData<String?>()
    val emitCollectCheckinData = MutableLiveData<Boolean?>()
    val emitCheckinSucess = MutableLiveData<Boolean?>()
    val emitLoading = MutableLiveData<Boolean>(false)

    val inputName = MutableLiveData<String>()
    val inputEmail = MutableLiveData<String>()

    var eventId: Int = 0

    fun startDirections(event: Event) {
        val result = eventDirectionUseCase.execute(event.toDomainEvent())


        result.foldExecute({
            emitError.value = it.message
        }, {
            emitDirection.value = it
        })
    }

    fun checkin() {
        emitLoading.value = true
        viewModelScope.launch {
            val result = checkinUseCase.execute(
                CheckinRequest(
                    eventId = eventId,
                    name = inputName.value ?: "",
                    email = inputEmail.value ?: ""
                )
            )

            withContext(mainDispatcher) {
                emitLoading.value = false

                result.foldExecute({
                    emitError.value = it.message
                }, {
                    emitCheckinSucess.value = true
                })

            }
        }
    }

    fun collectCheckinData() {
        emitCollectCheckinData.value = true
    }

    fun share(event: Event, bitmap: Bitmap, externalCacheDir: File, context: Context) {
        viewModelScope.launch {
            val result = shareContentUseCase.execute(
                event.toDomainEvent(),
                bitmap,
                externalCacheDir,
                context
            )

            withContext(mainDispatcher) {

                result.foldExecute({
                    emitError.value = it.message
                }, {
                    emitShare.value = it
                })

            }
        }
    }
}