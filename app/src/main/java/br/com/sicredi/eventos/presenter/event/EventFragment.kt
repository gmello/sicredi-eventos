package br.com.sicredi.eventos.presenter.event

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.drawable.toBitmap
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.navigation.NavOptions
import androidx.navigation.fragment.navArgs
import br.com.sicredi.eventos.BR
import br.com.sicredi.eventos.R
import br.com.sicredi.eventos.databinding.EventBinding
import br.com.sicredi.eventos.presenter.BaseFragment
import com.google.android.material.color.MaterialColors
import com.google.android.material.transition.MaterialContainerTransform
import org.koin.androidx.viewmodel.ext.android.viewModel


class EventFragment : BaseFragment<EventViewModel, EventBinding>() {

    private val viewModel: EventViewModel by viewModel<EventViewModel>()
    private val args: EventFragmentArgs by navArgs()
    private val bottomSheet by lazy {
        CheckinBottomSheet(viewModel)
    }

    override fun layoutId(): Int {
        return R.layout.fragment_event
    }

    override fun viewModelVariableId(): Int {
        return BR.viewModel
    }

    override fun viewModel(): EventViewModel {
        return viewModel
    }

    override fun getToolbar(): Toolbar {
        return binding!!.toolbar
    }

    override fun extraVariables(): Map<Int, Any>? {
        val map = HashMap<Int, Any>()
        map[BR.event] = args.event
        return map
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.let { bind ->
            bind.shareButton.setOnClickListener {
                val bitmap = bind.imageEvent.drawable.toBitmap()
                viewModel.share(
                    args.event,
                    bitmap,
                    requireActivity().externalCacheDir!!,
                    requireContext()
                )
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bind.coordinator.transitionName = args.event.id
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = buildContainerTransform(true)
        sharedElementReturnTransition = buildContainerTransform(false)

        args.event.id.toIntOrNull()?.let {
            viewModel.eventId = it
        }

        viewModel.emitError.observe(this) { message ->
            message?.let {
                showError(it)
            }
        }

        viewModel.emitDirection.observe(this) { intent ->
            intent?.let {
                startActivity(it)
            }
        }

        viewModel.emitShare.observe(this) { intent ->
            intent?.let {
                startActivity(it)
            }
        }

        viewModel.emitCollectCheckinData.observe(this){ collect ->
            collect?.let {
                if(it)
                    bottomSheet.show(parentFragmentManager, CheckinBottomSheet.TAG)
            }
        }

        viewModel.emitCheckinSucess.observe(this){ checkin ->
            checkin?.let {
                if(it) {
                    bottomSheet.dismiss()
                    navigator.navigate(EventFragmentDirections.actionEventFragmentToCheckinSuccessFragment())
                }
            }
        }

    }

    private fun buildContainerTransform(entering: Boolean) =
        MaterialContainerTransform(requireContext(), entering).apply {
            drawingViewId = R.id.nav_host_fragment
            interpolator = FastOutSlowInInterpolator()
            containerColor = MaterialColors.getColor(
                requireActivity().findViewById(android.R.id.content),
                com.google.android.material.R.attr.colorSurface
            )
            fadeMode = MaterialContainerTransform.FADE_MODE_OUT
            duration = 300
        }
}