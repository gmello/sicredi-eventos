package br.com.sicredi.eventos.domain.home

import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.core.left
import br.com.sicredi.eventos.core.rigth
import br.com.sicredi.eventos.data.IEventsRepository
import br.com.sicredi.eventos.domain.model.Event

class HomeGetEventsUseCase(
    private val repository: IEventsRepository
) : IHomeGetEventsUseCase {
    override suspend fun execute(): Either<Exception, List<Event>> {
        val result = repository.getEvents()

        return result.fold({
            left(it)
        }, {
            rigth(it.map { eventResponse -> Event.from(eventResponse = eventResponse) })
        })
    }
}