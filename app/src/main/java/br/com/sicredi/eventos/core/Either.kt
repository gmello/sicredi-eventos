package br.com.sicredi.eventos.core

sealed class Either<L, R> {
    fun isLeft(): Boolean {
        return this is Left<*, *>
    }

    fun isRigth(): Boolean {
        return this is Right<*, *>
    }

    fun <LL, RR> fold(l: (L) -> Either<LL, RR>, r: (R) -> Either<LL, RR>): Either<LL, RR> {
        return when (this) {
            is Left<*, *> -> l((this as Left<L, *>).value)
            is Right<*, *> -> r((this as Right<*, R>).value)
        }
    }

    fun foldExecute(l: (L) -> Unit, r: (R) -> Unit) {
        when (this) {
            is Left<*, *> -> l((this as Left<L, *>).value)
            is Right<*, *> -> r((this as Right<*, R>).value)
        }
    }
}

private data class Left<L, R>(val value: L) : Either<L, Nothing>()
private data class Right<L, R>(val value: R) : Either<Nothing, R>()

fun <L, R> left(l: L): Either<L, R> {
    return Left<L, R>(l) as Either<L, R>
}

fun <L, R> rigth(r: R): Either<L, R> {
    return Right<L, R>(r) as Either<L, R>
}