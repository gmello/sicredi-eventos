package br.com.sicredi.eventos.domain.event

import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.core.left
import br.com.sicredi.eventos.core.rigth
import br.com.sicredi.eventos.data.IEventsRepository
import br.com.sicredi.eventos.domain.model.Event

class EventDetailsUseCase(
    private val repository: IEventsRepository
) : IEventDetailsUseCase {
    override suspend fun execute(id: Int): Either<Exception, Event> {
        if (id <= 0)
            return left(Exception("Código de evento inválido!"))

        val result = repository.getEventDetails(id)

        return result.fold({
            left(it)
        }, {
            rigth(Event.from(it))
        })
    }
}