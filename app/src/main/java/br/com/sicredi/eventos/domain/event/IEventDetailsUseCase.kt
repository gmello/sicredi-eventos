package br.com.sicredi.eventos.domain.event

import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.domain.model.Event

interface IEventDetailsUseCase {
    suspend fun execute(id: Int): Either<Exception, Event>
}