# Sicredi Eventos 

<p>Aplicativo de listagem de eventos, detalhes e check-in.</p> 
<br />

## Arquitetura

<p>Desenvolvido utilizando arquitetura MVVM, buscando introduzir conceitos de Clean Arch, trazendo a responsabilidade única para classes, separação das camadas e desacoplamento.</p>

- Data: Camada de dados, onde separei as classes de comunicação com a API, modelos de dados referentes as requisições e utilitarios pertinentes a esta camada. 

- Domain: Camada de dominio, onde separei lógicas de negócio, em use cases especificas para cada ação.

- Presenter: Cada de view, onde separei a view e viewmodel, onde viewmodel faz a conexão com as regras de negocio e fornece os dados resultantes das ações para a view, que tem como objetivo apenas apresentar os dados.


## Bibliotecas

```
  //Live Data
  implementation 'androidx.lifecycle:lifecycle-livedata-ktx:2.4.1'

  //Viewmodel
  implementation 'androidx.lifecycle:lifecycle-viewmodel-ktx:2.4.1'

  //Navigation component
  implementation "androidx.navigation:navigation-fragment-ktx:2.4.2"
  implementation "androidx.navigation:navigation-ui-ktx:2.4.2"
  androidTestImplementation "androidx.navigation:navigation-testing:2.4.2"

  //Koin DI
  implementation "io.insert-koin:koin-core:3.1.3"
  implementation "io.insert-koin:koin-android:3.1.3"

  //Retrofit
  implementation 'com.squareup.retrofit2:converter-gson:2.8.1'
  implementation 'com.squareup.retrofit2:retrofit:2.8.1'
  implementation 'com.google.code.gson:gson:2.8.9'

  //Picasso
  implementation 'com.squareup.picasso:picasso:2.8'

  //Lottie
  implementation 'com.airbnb.android:lottie:5.1.1'
```

- <b>LiveData</b>: Utilizado para reatividade de dados, a view observa alterações em componentes LiveData na ViewModel e se atualiza de acordo a tal.
- <b>ViewModel</b>: Componente para organização dos dados relacionados a view, ligação entre view e objetos de negocio.
- <b>Navigation</b>: Componente de navegação, permite a organização do gráfico de roteamento. Componente acaba conduzino o desenvolvimento para o padrão de single activity.
- <b>Koin</b>: Injeção de dependência. Optei pelo Koin pela simplicidade da utilização. Estou ciente sobre o Hilt, porém ainda não utilizei, mas tive experiencia com Dagger2.
- <b>Retrofit (+ Okhttp + gson)</b>: Parte de consumo de api, nos auxilia abstraindo a parte de consumo da api, nos permitindo focar na logica de validação e transformação. Também junto esta vinculado com okhttp e gson para parse dos objetos JSON.
- <b>Picasso</b>: Biblioteca para loading de imagens view network com cache.
- <b>Lottie</b>: Biblioteca de loading de vetores animados.