package br.com.sicredi.eventos.domain.checkin

import androidx.core.util.PatternsCompat
import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.core.left
import br.com.sicredi.eventos.data.IEventsRepository
import br.com.sicredi.eventos.data.model.CheckinRequest
import br.com.sicredi.eventos.data.model.CheckinResponse

class CheckinUseCase(
    private val repository: IEventsRepository
) : ICheckinUseCase {
    override suspend fun execute(request: CheckinRequest): Either<Exception, CheckinResponse> {

        if (request.eventId <= 0)
            return left(Exception("Código do evento não foi informado!"))

        if (request.name.isEmpty())
            return left(Exception("Preencha o nome!"))

        if (PatternsCompat.EMAIL_ADDRESS.matcher(request.email).matches().not())
            return left(Exception("Email inválido, verifique por favor!"))

        return repository.checkin(request)
    }
}