package br.com.sicredi.eventos.domain.event

import android.content.Intent
import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.domain.model.Event

interface IEventDirectionUseCase {
    fun execute(event: Event): Either<Exception, Intent>
}