package br.com.sicredi.eventos.data

import br.com.sicredi.eventos.data.model.CheckinRequest
import br.com.sicredi.eventos.data.model.CheckinResponse
import br.com.sicredi.eventos.data.model.EventResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface IEventsApi {

    @GET("events")
    suspend fun getEvents(): Response<List<EventResponse>>

    @GET("events/{id}")
    suspend fun getEventDetails(@Path("id") id: Int) : Response<EventResponse>

    @POST("checkin")
    suspend fun checkin(@Body request: CheckinRequest): Response<CheckinResponse>

}