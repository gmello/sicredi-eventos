package br.com.sicredi.eventos.presenter.home.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.sicredi.eventos.databinding.ItemEventBinding
import br.com.sicredi.eventos.presenter.model.Event
import com.google.android.material.card.MaterialCardView

class EventsAdapter(
    private val dataSet: List<Event>,
    private val onSelectEventListener: OnSelectEventListener
) :
    RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val itemEventBinding: ItemEventBinding =
            ItemEventBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
            )
        return ViewHolder(itemEventBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.binding.event = dataSet[position]
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewHolder.binding.cardEvent.transitionName = dataSet[position].id
        }
        viewHolder.binding.root.setOnClickListener {
            onSelectEventListener.onSelect(dataSet[position], viewHolder.binding.cardEvent)
        }
    }

    override fun getItemCount() = dataSet.size

    class ViewHolder(val binding: ItemEventBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }

    interface OnSelectEventListener{
        fun onSelect(event: Event, card: MaterialCardView)
    }
}
