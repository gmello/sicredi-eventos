package br.com.sicredi.eventos.presenter

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import br.com.sicredi.eventos.R

abstract class BaseFragment<V : ViewModel, B : ViewDataBinding> : Fragment() {

    var binding: B? = null

    val navigator by lazy {
        this.findNavController()
    }

    @LayoutRes
    abstract fun layoutId(): Int

    abstract fun viewModelVariableId(): Int
    abstract fun viewModel(): V
    abstract fun extraVariables(): Map<Int, Any>?
    abstract fun getToolbar(): Toolbar?

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(layoutInflater, layoutId(), container, false)
        binding?.setVariable(viewModelVariableId(), viewModel())

        extraVariables()?.forEach {
            binding?.setVariable(it.key, it.value)
        }

        binding?.root?.setOnClickListener {
            hideKeyboard()
        }

        getToolbar()?.let {
            (activity as AppCompatActivity).setSupportActionBar(it)

            val navFragment =
                requireActivity().supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
            val appBarConfiguration = AppBarConfiguration(navFragment.navController.graph)

            setupActionBarWithNavController(
                activity as AppCompatActivity,
                navFragment.navController,
                appBarConfiguration
            )
        }

        binding?.lifecycleOwner = viewLifecycleOwner
        binding?.executePendingBindings()
        return binding?.root
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager? =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(binding?.root?.windowToken, 0)
    }

    fun showProgressBar() {
        activity?.let {
            it.findViewById<ProgressBar>(R.id.progressBar).visibility = View.VISIBLE
        }
    }

    fun hideProgressBar() {
        activity?.let {
            it.findViewById<ProgressBar>(R.id.progressBar).visibility = View.GONE
        }
    }

    fun showError(message: String) {
        if (message.isEmpty())
            return

        AlertDialog.Builder(requireContext())
            .setTitle(R.string.error)
            .setMessage(message)
            .setIcon(R.drawable.ic_baseline_error_outline_24)
            .setPositiveButton(R.string.ok, null)
            .show()
    }

}