package br.com.sicredi.eventos.domain.home

import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.domain.model.Event

interface IHomeGetEventsUseCase {
    suspend fun execute(): Either<Exception, List<Event>>
}