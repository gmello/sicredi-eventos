package br.com.sicredi.eventos.data.model

import com.google.gson.annotations.SerializedName

data class CheckinRequest (
    @SerializedName("eventId")
    val eventId: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("email")
    val email: String
)
