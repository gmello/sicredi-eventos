package br.com.sicredi.eventos

import br.com.sicredi.eventos.core.rigth
import br.com.sicredi.eventos.data.IEventsRepository
import br.com.sicredi.eventos.data.model.EventResponse
import br.com.sicredi.eventos.domain.home.IHomeGetEventsUseCase
import br.com.sicredi.eventos.domain.model.Event
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test

@ExperimentalCoroutinesApi
class MappingUnitTest {

    @Test
    fun `assert domain event are correctly mapped to presenter event`() = runBlockingTest {
        val homeGetEventsUseCase = mockk<IHomeGetEventsUseCase>()

        coEvery { homeGetEventsUseCase.execute() } returns rigth(
            listOf(
                Event(
                    people = emptyList(),
                    date = 123,
                    description = "test",
                    image = "test",
                    longitude = 1.0,
                    latitude = 1.0,
                    price = 1.0,
                    title = "test",
                    id = "test"
                ),
            )
        )

        val result = homeGetEventsUseCase.execute()
        var presenterEvent: br.com.sicredi.eventos.presenter.model.Event

        result.foldExecute({}, {
            presenterEvent = br.com.sicredi.eventos.presenter.model.Event.from(it[0])

            assertEquals(
                "longitude data its not correctly mapped",
                presenterEvent.longitude,
                it[0].longitude,
                0.0,
            )

            assertEquals(
                "latitude data its not correctly mapped",
                presenterEvent.latitude,
                it[0].latitude,
                0.0,
            )

            assertEquals(
                "price data its not correctly mapped",
                presenterEvent.price,
                it[0].price,
                0.0,
            )

            assertEquals(
                "date data its not correctly mapped",
                presenterEvent.date,
                it[0].date,
            )

            assertEquals(
                "description data its not correctly mapped",
                presenterEvent.description,
                it[0].description,
            )

            assertEquals(
                "image data its not correctly mapped",
                presenterEvent.image,
                it[0].image,
            )

            assertEquals(
                "title data its not correctly mapped",
                presenterEvent.title,
                it[0].title,
            )

            assertEquals(
                "id data its not correctly mapped",
                presenterEvent.id,
                it[0].id,
            )
        })

    }

    @Test
    fun `assert data event are correctly mapped to domain event`() = runBlockingTest {
        val eventRepository = mockk<IEventsRepository>()

        coEvery { eventRepository.getEvents() } returns rigth(
            listOf(
                EventResponse(
                    people = emptyList(),
                    date = 123,
                    description = "test",
                    image = "test",
                    longitude = 1.0,
                    latitude = 1.0,
                    price = 1.0,
                    title = "test",
                    id = "test"
                ),
            )
        )

        val result = eventRepository.getEvents()
        var domainEvent: Event

        result.foldExecute({}, {
            domainEvent = Event.from(it[0])

            assertEquals(
                "longitude data its not correctly mapped",
                domainEvent.longitude,
                it[0].longitude,
                0.0,
            )

            assertEquals(
                "latitude data its not correctly mapped",
                domainEvent.latitude,
                it[0].latitude,
                0.0,
            )

            assertEquals(
                "price data its not correctly mapped",
                domainEvent.price,
                it[0].price,
                0.0,
            )

            assertEquals(
                "date data its not correctly mapped",
                domainEvent.date,
                it[0].date,
            )

            assertEquals(
                "description data its not correctly mapped",
                domainEvent.description,
                it[0].description,
            )

            assertEquals(
                "image data its not correctly mapped",
                domainEvent.image,
                it[0].image,
            )

            assertEquals(
                "title data its not correctly mapped",
                domainEvent.title,
                it[0].title,
            )

            assertEquals(
                "id data its not correctly mapped",
                domainEvent.id,
                it[0].id,
            )
        })

    }

}