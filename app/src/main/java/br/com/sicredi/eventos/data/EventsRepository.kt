package br.com.sicredi.eventos.data

import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.core.left
import br.com.sicredi.eventos.data.model.CheckinRequest
import br.com.sicredi.eventos.data.model.CheckinResponse
import br.com.sicredi.eventos.data.model.EventResponse
import br.com.sicredi.eventos.data.utils.ResponseValidator

class EventsRepository(
    private val api: IEventsApi
) : IEventsRepository {

    override suspend fun getEvents(): Either<Exception, List<EventResponse>> {
        return try {
            val response = api.getEvents()

            ResponseValidator.validate<List<EventResponse>>(response)
        } catch (e: Exception) {
            left(e)
        }
    }

    override suspend fun getEventDetails(id: Int): Either<Exception, EventResponse> {
        return try {
            val response = api.getEventDetails(id)

            ResponseValidator.validate<EventResponse>(response)
        } catch (e: Exception) {
            left(e)
        }
    }

    override suspend fun checkin(request: CheckinRequest): Either<Exception, CheckinResponse> {
        return try {
            val response = api.checkin(request)

            ResponseValidator.validate<CheckinResponse>(response)
        } catch (e: Exception) {
            left(e)
        }
    }
}