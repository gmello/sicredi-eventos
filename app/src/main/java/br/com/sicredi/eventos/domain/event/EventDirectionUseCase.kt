package br.com.sicredi.eventos.domain.event

import android.content.Intent
import android.net.Uri
import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.core.rigth
import br.com.sicredi.eventos.domain.model.Event

class EventDirectionUseCase : IEventDirectionUseCase {
    override fun execute(event: Event): Either<Exception, Intent> {
        val gmmIntentUri: Uri =
            Uri.parse("google.navigation:q=${event.latitude},${event.longitude}")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")

        return rigth(mapIntent)
    }
}