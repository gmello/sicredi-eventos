package br.com.sicredi.eventos.domain.model

import br.com.sicredi.eventos.data.model.EventResponse
import java.text.SimpleDateFormat
import java.util.*

data class Event(
    val people: List<Any?>,
    val date: Long,
    val description: String,
    val image: String,
    val longitude: Double,
    val latitude: Double,
    val price: Double,
    val title: String,
    val id: String
){
    companion object {
        fun from(eventResponse: EventResponse): Event{
            return Event(
                people = eventResponse.people,
                date = eventResponse.date,
                description = eventResponse.description,
                image = eventResponse.image,
                longitude = eventResponse.longitude,
                latitude = eventResponse.latitude,
                price = eventResponse.price,
                title = eventResponse.title,
                id = eventResponse.id,
            )
        }
    }

    fun dateFormated(): String{
        val dateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault())
        return dateFormat.format(Date(date))
    }
}