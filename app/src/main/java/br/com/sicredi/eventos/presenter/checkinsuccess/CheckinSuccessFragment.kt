package br.com.sicredi.eventos.presenter.checkinsuccess

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import br.com.sicredi.eventos.BR
import br.com.sicredi.eventos.R
import br.com.sicredi.eventos.databinding.CheckinSuccessBinding
import br.com.sicredi.eventos.presenter.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel


class CheckinSuccessFragment : BaseFragment<CheckinSuccessViewModel, CheckinSuccessBinding>() {

    private val viewModel: CheckinSuccessViewModel by viewModel<CheckinSuccessViewModel>()

    override fun layoutId(): Int {
        return R.layout.fragment_checkin_success
    }

    override fun viewModelVariableId(): Int {
        return BR.viewModel
    }

    override fun viewModel(): CheckinSuccessViewModel {
        return viewModel
    }

    override fun extraVariables(): Map<Int, Any>? {
        return null
    }

    override fun getToolbar(): Toolbar? {
        return null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.emitDone.observe(this){
            if(it)
                navigator.navigate(CheckinSuccessFragmentDirections.actionCheckinSuccessFragmentToHomeFragment())
        }
    }


}