package br.com.sicredi.eventos

import androidx.multidex.MultiDexApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.fragment.koin.fragmentFactory
import org.koin.core.context.startKoin

class MainApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MainApplication)
            fragmentFactory()
            modules(
                listOf(
                    viewModelModule,
                    networkModule,
                    repositoryModule,
                    userCaseModule,
                    coroutinesModule,
                )
            )
        }
    }

}