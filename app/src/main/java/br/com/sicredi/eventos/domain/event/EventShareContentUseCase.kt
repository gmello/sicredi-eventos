package br.com.sicredi.eventos.domain.event

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.text.Html
import androidx.core.content.FileProvider
import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.core.left
import br.com.sicredi.eventos.core.rigth
import br.com.sicredi.eventos.domain.model.Event
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException


class EventShareContentUseCase : IEventShareContentUseCase {
    override suspend fun execute(
        event: Event,
        bitmap: Bitmap,
        externalCacheDir: File,
        context: Context
    ): Either<Exception, Intent> {
        try {
            val file = File(externalCacheDir, "share_image_event.jpeg")
            val fout = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fout)
            fout.flush()
            fout.close()
            file.setReadable(true, false)

            val intent = Intent(Intent.ACTION_SEND)
            val fileUri = FileProvider.getUriForFile(
                context,
                "br.com.sicredi.fileprovider",
                file);

            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.type = "*/*"
            intent.putExtra(Intent.EXTRA_STREAM, fileUri)
            intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(StringBuilder()
                .append("<p><b>${event.title}</b></p>")
                .append("<small><p>${event.description.substring(0, 100)}...</p></small>")
                .append("<b>${event.dateFormated()}</b>")
                .toString())
            )
            return rigth(intent)
        } catch (e: FileNotFoundException) {
            return left(e)
        } catch (e: IOException) {
            return left(e)
        } catch (e: Exception) {
            return left(e)
        }
    }
}