package br.com.sicredi.eventos.presenter.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Event(
    val date: Long,
    val description: String,
    val image: String,
    val price: Double,
    val title: String,
    val longitude: Double,
    val latitude: Double,
    val id: String
): Parcelable {
    companion object {
        fun from(event: br.com.sicredi.eventos.domain.model.Event): Event {
            return Event(
                date = event.date,
                description = event.description,
                image = event.image,
                price = event.price,
                title = event.title,
                id = event.id,
                latitude = event.latitude,
                longitude = event.longitude
            )
        }
    }

    fun dayOfMonth(): String{
        val dateFormat = SimpleDateFormat("dd", Locale.getDefault())
        return dateFormat.format(Date(date))
    }

    fun monthName(): String {
        val dateFormat = SimpleDateFormat("MMM", Locale.getDefault())
        return dateFormat.format(Date(date))
    }

    fun hour(): String {
        val dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        return dateFormat.format(Date(date))
    }

    fun staticMap(): String{
        return "https://maps.googleapis.com/maps/api/staticmap?center=${latitude},${longitude}&zoom=15&size=400x200&markers=color:red|label:${title}|${latitude},${longitude}&key=AIzaSyCyMD7bjZyyMnKg22pwY9ku1XEfXdrsPo0"
    }

    fun priceFormated(): String {
        val format = NumberFormat.getCurrencyInstance(Locale.getDefault())
        return format.format(price)
    }

    fun toDomainEvent(): br.com.sicredi.eventos.domain.model.Event {
        return br.com.sicredi.eventos.domain.model.Event(
            people = emptyList(),
            date = this.date,
            description = this.description,
            image = this.image,
            price = this.price,
            title = this.title,
            id = this.id,
            latitude = this.latitude,
            longitude = this.longitude
        )
    }
}