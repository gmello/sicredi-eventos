package br.com.sicredi.eventos.data.model

import com.google.gson.annotations.SerializedName

data class CheckinResponse(
    @SerializedName("code") val code: Int
)