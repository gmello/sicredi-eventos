package br.com.sicredi.eventos

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import br.com.sicredi.eventos.presenter.MainActivity
import com.adevinta.android.barista.interaction.BaristaClickInteractions.clickOn
import com.adevinta.android.barista.interaction.BaristaEditTextInteractions.writeTo
import com.adevinta.android.barista.interaction.BaristaListInteractions.clickListItem
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class CheckinHappyPathInstrumentedTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkin_at_first_event() {
        Thread.sleep(2500)

        clickListItem(R.id.events, 0)

        clickOn(R.id.btnCheckin)

        writeTo(R.id.inputName, "Gustavo")
        writeTo(R.id.inputEmail, "gustavo.schneider3@gmail.com")

        clickOn(R.id.btnSendCheckin)

        Thread.sleep(2000)

        clickOn(R.id.btnDone)
    }
}