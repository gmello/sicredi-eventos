package br.com.sicredi.eventos.domain.event

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import br.com.sicredi.eventos.core.Either
import br.com.sicredi.eventos.domain.model.Event
import java.io.File

interface IEventShareContentUseCase {
    suspend fun execute(event: Event, bitmap: Bitmap, externalCacheDir: File, context: Context): Either<Exception, Intent>
}