package br.com.sicredi.eventos.presenter.home

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.sicredi.eventos.BR
import br.com.sicredi.eventos.R
import br.com.sicredi.eventos.databinding.HomeBinding
import br.com.sicredi.eventos.presenter.BaseFragment
import br.com.sicredi.eventos.presenter.home.adapter.EventsAdapter
import br.com.sicredi.eventos.presenter.model.Event
import com.google.android.material.card.MaterialCardView
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<HomeViewModel, HomeBinding>() {

    private val viewModel: HomeViewModel by viewModel<HomeViewModel>()

    override fun layoutId(): Int {
        return R.layout.fragment_home
    }

    override fun viewModelVariableId(): Int {
        return BR.viewModel
    }

    override fun viewModel(): HomeViewModel {
        return viewModel
    }

    override fun extraVariables(): Map<Int, Any>? {
        return null
    }

    override fun getToolbar(): Toolbar {
        return binding!!.toolbar
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        viewModel.loadEvents()

        viewModel.emitLoading.observe(this) {
            if (it)
                showProgressBar()

            if (it.not())
                hideProgressBar()
        }

        viewModel.emitError.observe(this) {
            showError(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.emitEvents.observe(viewLifecycleOwner) {
            binding?.let { view ->
                val adapter = EventsAdapter(it, object : EventsAdapter.OnSelectEventListener {
                    override fun onSelect(event: Event, card: MaterialCardView) {
                        val extras = FragmentNavigatorExtras(
                            card to event.id.toString()
                        )
                        val action = HomeFragmentDirections.actionHomeFragmentToEventFragment(event)
                        navigator.navigate(action, extras)
                    }
                })
                view.events.layoutManager = LinearLayoutManager(context)
                view.events.adapter = adapter
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_update)
            viewModel.loadEvents()
        return super.onOptionsItemSelected(item)
    }

}